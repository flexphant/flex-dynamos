import Link from 'next/link'

const Navbar = () => (
    <nav>
        <ul>
            <li><Link href="/"><a>Home</a></Link></li>
            <li><Link href="/about"><a>About</a></Link></li>
            <li><Link href="/contact"><a>Contact</a></Link></li>
            <li><Link href="/registration"><a>Register</a></Link></li>
        </ul>
    </nav>
)

export default Navbar