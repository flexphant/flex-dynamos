import Layout from '../components/layout'

const HomePage = () => (
    <Layout>
        <div>Welcome to our home page</div>
    </Layout>
)

export default HomePage