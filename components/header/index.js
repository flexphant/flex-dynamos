import Navbar from '../navbar'

const Header = () => (
    <div>
        <Navbar />
         {/** More header content goes in here**/}
    </div>
)

export default Header