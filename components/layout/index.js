// import components
import Header from '../header'

// import bootstrap css
import '../../scss/bootstrap.scss'

const Layout = (props) => (
    <div>
        <Header />
        <div>
            {props.children}
        </div>
    </div>
)

export default Layout